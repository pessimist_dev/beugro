<?php

namespace App\Client;

use App\Exception\IcndbNotAvailableException;

class Icndb
{
    const BASE_URL = 'http://api.icndb.com/';

    /**
     * @param int $limit
     * @throws IcndbNotAvailableException
     */
    public function getRandomJokes($limit = 1)
    {
        $url = static::BASE_URL.'jokes/random/'.$limit;

        $c = curl_init();
        curl_setopt($c, CURLOPT_USERAGENT, "chuck-norris-php client");
        curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($c, CURLOPT_TIMEOUT, 5);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($c, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($c, CURLOPT_URL, $url);

        $response = curl_exec($c);
        $code = curl_getinfo($c, CURLINFO_HTTP_CODE);

        if ($code != 200 || empty($response)) {
            throw new IcndbNotAvailableException('Icndb is not available.');
        }

        $decoded = json_decode($response);

        if (!empty($decoded->value)) {
            return $decoded->value;
        }
    }
}
