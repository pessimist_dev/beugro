<?php

namespace App\Controller;

use App\Exception\NotAllowedMergerException;
use App\Exception\SameProviderSourceException;
use App\Exception\TwitterException;
use App\Manager\MessageManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MessageController extends Controller
{
    /**
     * List api response messages, sorted by a given logic by the "mod" request parameter
     *
     * @param Request        $request
     * @param MessageManager $messageManager
     *
     * @return Response
     */
    public function listMessages(Request $request, MessageManager $messageManager)
    {
        try {
            $messages = $messageManager->getMessages(
                $request->get('slugA'),
                $request->get('slugB'),
                $request->get('mod', 'fib')
            );
        } catch (NotAllowedMergerException $e) {
            $error = $e->getMessage();
        } catch (SameProviderSourceException $e) {
            $error = $e->getMessage();
        } catch (TwitterException $e) {
            $error = $e->getMessage();
        } catch (\Exception $e) {
            $error = 'Something went wrong. Try it later.';
        }

        return $this->render(
            'list.html.twig',
            [
                'messages' => $messages ?? [],
                'error'    => $error ?? null,
            ]
        );
    }
}
