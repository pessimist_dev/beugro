<?php

namespace App\DependencyInjection\Compiler;

use App\Manager\MergerManager;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class MessageMergerPass implements CompilerPassInterface
{
    /**
     * Register message mergers to the merger manager
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition(MergerManager::class);

        foreach ($container->findTaggedServiceIds('message.merger') as $id => $tags) {
            $definition->addMethodCall('addMerger', array(new Reference($id)));
        }
    }
}
