<?php

namespace App\Exception;

/**
 * Exception class to define missing merger error
 */
class NotAllowedMergerException extends \Exception
{
}
