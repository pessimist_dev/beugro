<?php

namespace App\Helper;

class Fibonacci
{
    /**
     * Returns fibonacci numbers between an interval
     *
     * @param int $from
     * @param int $to
     *
     * @return array
     */
    public function generate($from = 3, $to = 10)
    {
        $fibonacci = [];
        $f = 0;
        $prev = 1;

        while ($to > $f) {
            $next = $f + $prev;
            $prev = $f;
            $f = $next;

            if ($f >= $from) {
                $fibonacci[] = $f;
            }
        }

        return $fibonacci;
    }
}
