<?php

namespace App\Manager;

use App\Exception\NotAllowedMergerException;
use App\Merger\MergerInterface;

/**
 * Class to use and handling different mergers logic
 */
class MergerManager
{
    /**
     * @var MergerInterface[]
     */
    protected $mergers;

    public function __construct()
    {
        $this->mergers = [];
    }

    /**
     * Register a message merger algorithm (as a compiler pass)
     *
     * @param MergerInterface $merger
     */
    public function addMerger(MergerInterface $merger)
    {
        $this->mergers[$merger->getAlias()] = $merger;
    }

    /**
     * Merge and return messages
     *
     * @param array  $tweetsA
     * @param array  $tweetsB
     * @param array  $icndbMessages
     * @param string $method
     *
     * @throws NotAllowedMergerException
     * @return array
     */
    public function getMerged(array $tweetsA = [], array $tweetsB = [], array $icndbMessages = [], $method = 'fib')
    {
        if (!$this->isValidMethod($method)) {
            throw new NotAllowedMergerException('The method: '.$method.' is not allowed.');
        }

        $tweets = $this->mergers['published_at']->getMerged($tweetsA, $tweetsB);

        return $this->mergers[$method]->getMerged($tweets, $icndbMessages);
    }

    /**
     * Return true if the given method is a valid merger method
     *
     * @param $method
     * @return boolean
     */
    public function isValidMethod($method)
    {
        return in_array($method, ['fib', 'mod']);
    }
}
