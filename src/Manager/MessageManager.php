<?php

namespace App\Manager;

use App\Exception\NotAllowedMergerException;
use App\Exception\SameProviderSourceException;
use App\Helper\Fibonacci;
use App\Provider\IcndbProvider;
use App\Provider\TwitterProvider;
use App\ValueObject\Message;

/**
 * Handle merger and api client providers compositions
 */
class MessageManager
{
    /**
     * @var MergerManager
     */
    protected $merger;

    /**
     * @var Fibonacci
     */
    protected $fibonacci;

    /**
     * @var IcndbProvider
     */
    protected $icndbProvider;

    /**
     * @var TwitterProvider
     */
    protected $twitterProvider;

    /**
     * @param TwitterProvider $twitterProvider
     * @param IcndbProvider   $icndbProvider
     * @param MergerManager   $merger
     * @param Fibonacci       $fibonacci
     */
    public function __construct(
        TwitterProvider $twitterProvider,
        IcndbProvider $icndbProvider,
        MergerManager $merger,
        Fibonacci $fibonacci
    )
    {
        $this->twitterProvider = $twitterProvider;
        $this->icndbProvider = $icndbProvider;
        $this->merger = $merger;
        $this->fibonacci = $fibonacci;
    }

    /**
     * Get messages from providers and merge them
     *
     * @param string $sourceA A twitter source slug
     * @param string $sourceB An other twitter source slug
     * @param string $method The merging method
     *
     * @throws SameProviderSourceException
     * @throws NotAllowedMergerException
     *
     * @return Message[]
     */
    public function getMessages($sourceA, $sourceB, $method = 'fib')
    {
        if ($sourceA == $sourceB) {
            throw new SameProviderSourceException('Same twitter source is not allowed');
        }

        if (!$this->merger->isValidMethod($method)) {
            throw new NotAllowedMergerException('The method: '.$method.' is not allowed.');
        }

        $messagesA = $this->twitterProvider->getMessages($sourceA, 20);
        $messagesB = $this->twitterProvider->getMessages($sourceB, 20);

        // Count the necessary icndb results
        $needed = count($messagesA) + count($messagesB);
        $icndbMessages = $this->icndbProvider->getMessages($needed);

        return $this->merger->getMerged($messagesA, $messagesB, $icndbMessages, $method);
    }
}
