<?php

namespace App\Merger;
use App\Helper\Fibonacci;

/**
 * Merging two array items by position fibonacci logic
 */
class FibonacciMerger implements MergerInterface
{
    /**
     * @var Fibonacci
     */
    protected $fibonacci;

    /**
     * @param Fibonacci $fibonacci
     */
    public function __construct(Fibonacci $fibonacci)
    {
        $this->fibonacci = $fibonacci;
    }

    /**
     * @inheritdoc
     */
    public function getMerged(array $a = [], array $b = [])
    {
        $merged = [];
        $fibonacci = $this->fibonacci->generate(3, count($a));

        foreach ($a as $key => $message) {
            if (in_array(count($merged) + 1, $fibonacci)) {
                $merged[] = array_pop($b);
            }

            $merged[] = $message;

            unset($a[$key]);
        }

        return $merged;
    }

    /**
     * @inheritdoc
     */
    public function getAlias()
    {
        return 'fib';
    }
}
