<?php

namespace App\Merger;

use App\ValueObject\Message;

interface MergerInterface
{
    /**
     * @param Message[] $a
     * @param Message[] $b
     *
     * @return Message[]
     */
    public function getMerged(array $a = [], array $b = []);

    /**
     * The alias identify this algorithm
     *
     * @return string
     */
    public function getAlias();
}
