<?php

namespace App\Merger;
use App\ValueObject\Message;

/**
 * Merge two arrays of messages by their items' published date
 */
class PublishedAtMerger implements MergerInterface
{
    /**
     * @inheritdoc
     */
    public function getMerged(array $a = [], array $b = [])
    {
        /** @var $a Message[] */
        /** @var $b Message[] */
        if (!$a) {
            return $b;
        }

        if (!$b) {
            return $a;
        }

        $merged = [];

        $this
            ->shortByTime($a)
            ->shortByTime($b)
        ;

        foreach ($a as $keyA => $messageA) {
            // While the compare array's messageB items published dates earlier then the current messageA, merge them
            foreach ($b as $keyB => $messageB) {
                if ($messageB->getPublishedAt() >= $messageA->getPublishedAt()) {
                    $merged[] = $messageB;

                    // Remove this item, it's already handled
                    unset($b[$keyB]);
                } else {
                    break 1;
                }
            }

            $merged[] = $messageA;
            unset($a[$keyA]);
        }

        return $merged;
    }

    /**
     * Short messages in revers order
     *
     * @param Message[] $items
     * @return $this
     */
    public function shortByTime(array &$items)
    {
        usort(
            $items,
            function(Message $a, Message $b) {
                if ($a->getPublishedAt() == $b->getPublishedAt()) {
                    return 0;
                }

                return ($a->getPublishedAt() > $b->getPublishedAt()) ? -1 : 1;
            }
        );

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getAlias()
    {
        return 'published_at';
    }
}
