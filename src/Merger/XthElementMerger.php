<?php

namespace App\Merger;

/**
 * Merge two arrays by every x. element
 */
class XthElementMerger implements MergerInterface
{
    /**
     * @var int
     */
    protected $mod;

    /**
     * @param int $mod
     */
    public function __construct($mod = 3)
    {
        $this->mod = $mod;
    }

    /**
     * @inheritdoc
     */
    public function getMerged(array $a = [], array $b = [])
    {
        $merged = [];

        foreach ($a as $key => $message) {
            $merged[] = $message;

            if ((count($merged) + 1) % $this->mod === 0) {
                $merged[] = array_pop($b);
            }

            unset($a[$key]);
        }

        return $merged;
    }

    /**
     * @inheritdoc
     */
    public function getAlias()
    {
        return 'mod';
    }
}
