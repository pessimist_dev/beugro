<?php

namespace App\Provider;

use App\Client\Icndb;
use App\ValueObject\Message;

class IcndbProvider implements ProviderInterface
{
    /**
     * The ICNDB api client
     */
    protected $client;

    /**
     * @param Icndb $client
     */
    public function __construct(Icndb $client)
    {
        $this->client = $client;
    }

    /**
     * @inheritdoc
     */
    public function getMessages($limit = 20)
    {
        $jokes = $this->client->getRandomJokes($limit);
        $messages = [];

        foreach ($jokes as $joke) {
            $messages[] = new Message('icndb', $joke->joke);
        }

        return $messages;
    }
}
