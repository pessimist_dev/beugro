<?php

namespace App\Provider;

use App\ValueObject\Message;

interface ProviderInterface
{
    /**
     * Return the messages from a message source
     *
     * @param string $limit
     *
     * @return Message[]
     */
    public function getMessages($limit);
}
