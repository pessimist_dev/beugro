<?php

namespace App\Provider;

use App\Exception\TwitterException;
use App\ValueObject\Message;
use Endroid\Twitter\Client;

class TwitterProvider implements ProviderInterface
{
    /**
     * The twitter api client
     */
    protected $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * {@inheritdoc}
     * @throws TwitterException
     */
    public function getMessages($source, $limit = 20)
    {
        $messages = [];

        $results = $this->client->getClient()->get(
            'statuses/user_timeline',
            [
                'screen_name' => $source,
                'count' => $limit
            ]
        );

        if (!empty($results->errors)) {
            throw new TwitterException($results->errors[0]->message);
        }

        foreach ($results as $message) {
            $messages[] = new Message(
                'twitter/'.$source,
                $message->text,
                new \DateTime($message->created_at)
            );
        }

        return $messages;
    }
}
