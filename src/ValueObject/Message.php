<?php

namespace App\ValueObject;

/**
 * Class represents an api source message
 */
class Message
{
    /**
     * The source of the message
     *
     * @var string
     */
    protected $source;

    /**
     * The message created at
     *
     * @var \DateTime
     */
    protected $publishedAt;

    /**
     * The content of the message
     *
     * @var string
     */
    protected $content;

    /**
     * @param \DateTime $publishedAt
     * @param string    $source
     * @param string    $content
     */
    public function __construct($source, $content = '', \DateTime $publishedAt = null)
    {
        $this->publishedAt = $publishedAt;
        $this->source = $source;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}
